//Data in a var, as JSON properties
var data_001 = [
{"Concept":"University of Alabama System Office","Element":46220},
{"Concept":"Ualam","Element":40000},
{"Concept":"Amridge University","Element":33860},
{"Concept":"Alabama State University","Element":31030},
{"Concept":"Auburn University at Montgomery","Element":30276},
{"Concept":"Birmingham","Element":28978},
{"Concept":"Alabama A & M University","Element":26620},
{"Concept":"University of Alabama in Huntsville","Element":12234},
{"Concept":"Athens State","Element":11380},
{"Concept":"University of Alabama at Birmingham","Element":10820},
{"Concept":"Auburn University","Element":7030},
{"Concept":"Central Alabama Community College","Element":100}
];




// VARS TO EDIT

var pref_001 = "$";
var suf_001 = "m";

var barColor_001 = "#9FBBC1"
var barColor_Highlight_001 = "#23535E"

var highlighted = 2;

// END OF VARS TO EDIT





var nameNodes_001 = [];
var allValues_001 = [];
var allWidths_001 = [];
var maxValue_001, maxWidth_001, valuePos;


var jQInteractives = jQuery.noConflict();
(function( $ ) {
  $(function() {
   	
   	//Create Array with columns
  	for (var i in data_001[0]) {
		nameNodes_001.push(i);
	}

	//Get maximum value from Array
	for (var i in data_001) {
		allValues_001.push(data_001[i][nameNodes_001[1]])
	}
	maxValue_001 = Math.max.apply(null, allValues_001);

	//Create row with content for each item
	for (var i in data_001) {
		$("#barchart_001").append('<div class="barchart_row" id="bc_001_'+i+'">'+
				'<div class="nameCol">'+data_001[i][nameNodes_001[0]]+'</div>'+
				'<div class="barCol">'+
					'<div class="barWidth" id="bw_001_'+i+'" style="width:'+(data_001[i][nameNodes_001[1]]*100)/maxValue_001+'%; background:'+barColor_001+'">'+
						'<span class="barValue" id="bv_001_'+i+'">'+nWC_001(data_001[i][nameNodes_001[1]])+'</span>'+
					'</div>'+
				'</div>'+
			'</div>')
	}

	$("#bv_001_0").empty().append(pref_001+nWC_001(data_001[0][nameNodes_001[1]])+suf_001)

	//Find longest name of concept
	for (var i in data_001) {
		allWidths_001.push($('#bc_001_'+i+' .nameCol').width())
	}
	maxWidth_001 = Math.max.apply(null, allWidths_001);

	//Check size to decide widths of 
	checkSize_001();
	$(window).resize(function(){checkSize_001()})

	//Highlight bar
	$("#bc_001_"+(highlighted-1))
		.addClass("selectedBar_001")
	$("#bw_001_"+(highlighted-1))
		.css("background", barColor_Highlight_001)


	//Add commas to numbers in vars
	function nWC_001(x){
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	//Check size to decide widths
	function checkSize_001(){
		var w = $("#gf_001").width();
		var widthBarCol = (maxWidth_001*100)/w;

		if (widthBarCol < 40) {	
			$(".nameCol")
				.css("width", widthBarCol+"%")
				.css("margin-right", "2%");
		} else {
			$(".nameCol")
				.css("width", "30%")
				.css("margin-right", "2%");
		}


		$(".barCol")
			// .css("transition", "width 2s")
			.css("width", (98-widthBarCol)+"%");

		for (var i in data_001){
			var barw = $("#bw_001_"+i).width();
			if (barw > 50) {
				$("#bv_001_"+i).css("right", 5)
			} else {
				$("#bv_001_"+i).css("right", 0-5-($("#bv_001_"+i).width()) )
			}
		}
	}	

  });
})(jQInteractives);