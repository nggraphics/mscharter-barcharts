# Barcharts template

![Screen Shot 2015-11-24 at 4.26.38 PM.png](https://bitbucket.org/repo/74onL8/images/1586425749-Screen%20Shot%202015-11-24%20at%204.26.38%20PM.png)

- Based on NatGeo's [basic template](https://bitbucket.org/nggraphics/news-blank-template).

- Download this template and change the data and variables in `code.js` in the part highlightes as `VARS TO EDIT`.

- You'll be able to change color, highlight colors, highlighted bar (select 0 if none), prefix and suffix for units.

Please note: do not reproduce National Geographic logos or fonts without written permission.
